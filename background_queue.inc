<?php

/**
 * Async http request.
 */
function background_queue_async_http_request($url) {
  $url_info = parse_url($url);

  $is_https = ($url_info['scheme'] == 'https');
  $scheme = $is_https ? 'ssl://' : '';
  $port = isset($url_info['port']) ? $url_info['port'] : ($is_https ? 443 : 80);
  $query = isset($url_info['query']) ? '?' . $url_info['query'] : '';

  if (!$fp = fsockopen($scheme . $url_info['host'], $port)) {
    watchdog('background_queue', 'Socket open error.');
    return;
  }

  fwrite($fp, "GET {$url_info['path']}{$query} HTTP/1.1\r\n");
  fwrite($fp, "Host: {$url_info['host']}\r\n");
  fwrite($fp, "Connection: Close\r\n\r\n");
  fclose($fp);
}

/**
 * Process chunk Queue.
 */
function background_queue_process_callback($queue_name) {
  $queues = module_invoke_all('cron_queue_info');
  drupal_alter('cron_queue_info', $queues);

  if (!isset($queues[$queue_name])) {
    return;
  }

  $queue = DrupalQueue::get($queue_name);
  $queue_info = $queues[$queue_name];
  $queue_end = time() + (isset($queue_info['time']) ? $queue_info['time'] : 15);

  while (time() < $queue_end && ($item = $queue->claimItem())) {
    try {
      call_user_func($queue_info['worker callback'], $item->data);
      $queue->deleteItem($item);
    }
    catch (Exception $e) {
      watchdog_exception('background_queue', $e);
    }
  }

  if ($queue->numberOfItems() > 0) {
    background_queue_run($queue_name);
  }
}

/**
 * Test callback.
 */
function background_queue_test_callback() {
  watchdog('background_queue', 'Test');
}
